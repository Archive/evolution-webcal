/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2004-2007 Novell, Inc. (www.novell.com)
 *  Copyright 2008-2009 Rodney Dawes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef _E_WEBCAL_NOTIFY_H_
#define _E_WEBCAL_NOTIFY_H_

#include <libecal/e-cal.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libsoup/soup.h>

void e_webcal_display_error (const gchar * title, const gchar * message,
			     GtkWidget * parent);

void e_webcal_query_user (const gchar * name, const gchar * desc,
                          const gchar * caluri,
                          gboolean has_event, gboolean has_todo);

#endif
