/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2004-2007 Novell, Inc. (www.novell.com)
 *  Copyright 2008-2009 Rodney Dawes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include "evolution-webcal-notify.h"

static void e_webcal_open_cal_http (const gchar * uri, const gchar * old);

static SoupSession * session;

static gboolean e_webcal_has_kind (icalcomponent * comp, icalcomponent_kind kind)
{
  g_return_val_if_fail (comp != NULL, FALSE);

  return (icalcomponent_get_first_component (comp, kind) != NULL);
}

static void e_webcal_load (const gchar * body, const gchar * uri)
{
  icalcomponent * comp;
  icalproperty * prop;
  gint i, numprops;
  gchar * name = NULL, * desc = NULL;
  gboolean has_events, has_tasks;

  comp = icalparser_parse_string (body);
  if (comp == NULL) {
    SoupURI * tmpuri;
    gchar * message, * tmpname;

    tmpuri = soup_uri_new (uri);
    tmpname = g_path_get_basename (tmpuri->path);

    message = g_strdup_printf (_("There was an error parsing the calendar, "
				 "\"%s\". Please verify that it is a valid "
				 "calendar and try again."),
			       tmpname);

    e_webcal_display_error (_("Error Parsing Calendar"),
			       message,
			       NULL);

    g_free (tmpname);
    soup_uri_free (tmpuri);
    g_free (message);

    gtk_main_quit ();

    return;
  }

  numprops = icalcomponent_count_properties (comp, ICAL_X_PROPERTY);
  for (i = 0; i < numprops; i++) {
    if (i == 0) {
      prop = icalcomponent_get_first_property (comp, ICAL_X_PROPERTY);
    } else {
      prop = icalcomponent_get_next_property (comp, ICAL_X_PROPERTY);
    }
    if (prop != NULL) {
      const gchar * propname = icalproperty_get_x_name (prop);
      
      if (propname != NULL) {
	if (!strcmp (propname, "X-WR-CALNAME")) {
	  name = icalproperty_get_value_as_string_r (prop);
	} else if (!strcmp (propname, "X-WR-CALDESC")) {
	  desc = icalproperty_get_value_as_string_r (prop);
	}
      }
      icalproperty_free (prop);
    }
  }

  has_tasks  = e_webcal_has_kind (comp, ICAL_VTODO_COMPONENT);
  has_events = e_webcal_has_kind (comp, ICAL_VEVENT_COMPONENT);

  icalcomponent_free (comp);

  e_webcal_query_user (name, desc, uri, has_events, has_tasks);

  g_free (name);
  g_free (desc);

  gtk_main_quit ();
}

static void e_webcal_read (SoupSession *session, SoupMessage * msg, gpointer data) {
  const SoupURI * tmpuri;
  const gchar * uri = (const gchar *) data;
  const gchar * header;

  tmpuri = soup_message_get_uri (msg);

  if (SOUP_STATUS_IS_REDIRECTION (msg->status_code)) {
    header = soup_message_headers_get (msg->response_headers, "Location");
    if (header) {
      e_webcal_open_cal_http (header, uri);
    }
    return;
  }

  if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code)) {
    gchar * errorstring;
    gchar * errorname;

    errorname = g_path_get_basename (tmpuri->path);
    switch (msg->status_code) {
      /* Handle some common error codes here */
    case SOUP_STATUS_FORBIDDEN:
      errorstring = g_strdup_printf (_("Access to the calendar, \"%s\", "
				       "is forbidden."),
				     errorname);
      break;
    case SOUP_STATUS_NOT_FOUND:
      errorstring = g_strdup_printf (_("The calendar, \"%s\", was not "
				       "found on the server."),
				     errorname);
      break;
    case SOUP_STATUS_INTERNAL_SERVER_ERROR:
      errorstring = g_strdup_printf (_("There was an internal server error "
				       "while trying to load \"%s\"."),
				     errorname);
      break;
    default:
      errorstring = g_strdup_printf (_("There was an error loading the "
				       "calendar, \"%s\"."),
				     errorname);
      break;
    }
    e_webcal_display_error (_("Error Loading Calendar"),
			    errorstring,
			    NULL);
    g_free (errorname);
    g_free (errorstring);
    g_free (data);
    gtk_main_quit ();
    return;
  }

  e_webcal_load (msg->response_body->data, uri);

  g_free (data);
}

static void e_webcal_open_cal_http (const gchar * uri, const gchar * old) {
  SoupMessage * message;
  gchar * olduri;

  if (old != NULL) {
    olduri = g_strdup (old);
  } else {
    olduri = g_strdup (uri);
  }

  message = soup_message_new (SOUP_METHOD_GET, uri);
  if (!SOUP_IS_MESSAGE (message)) {
    gchar * errorstring;

    errorstring = g_strdup_printf (_("The URI \"%s\" is invalid."), olduri);
    e_webcal_display_error (_("Invalid URI Specified"),
			    errorstring,
			    NULL);
    g_free (errorstring);
    g_free (olduri);
    gtk_main_quit ();
    return;
  }

  soup_message_set_flags (message, SOUP_MESSAGE_NO_REDIRECT);

  soup_session_queue_message (session, message,
			      (SoupSessionCallback) e_webcal_read,
			      (gpointer) olduri);
}

static void e_webcal_open_cal (const gchar * uri) {
  gchar * newuri;

  if (strstr (uri, "webcal://")) {
    newuri = g_strdup_printf ("http://%s", uri + strlen ("webcal://"));
    e_webcal_open_cal_http (newuri, uri);
    g_free (newuri);
  } else if (!strstr (uri, "://")) {
    newuri = g_strconcat ("http://", uri, NULL);
    e_webcal_open_cal_http (newuri, newuri);
    g_free (newuri);
  } else {
    e_webcal_open_cal_http (uri, NULL);
  }
}

static gboolean e_webcal_idle_callback (void * data) {
  GSList * p = (GSList *) data;

  gtk_rc_parse_string ("style \"dialog-defaults\" {\n"
		       "  GtkDialog::action-area-border = 12\n"
		       "} widget_class \"GtkDialog\""
		       " style \"dialog-defaults\"\n");

  if (p != NULL) {
    for (; p && p->data; p = p->next) {
      const gchar * uri = p->data;

      e_webcal_open_cal (uri);
    }
    g_slist_free (p);
  } else {
    e_webcal_display_error (_("No URI Specified"),
			    _("No URI to load was specified. You need to "
			      "pass the URI of the calendar to subscribe "
			      "to as an argument on the command line."),
			    NULL);
    gtk_main_quit ();
  }

  return FALSE;
}

gint main (gint argc, gchar ** argv) {
  GSList * uri_list = NULL;
  static gchar **args;
  GError *error = NULL;

  static GOptionEntry options[] = {
    { G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_STRING_ARRAY, &args, NULL,  "[URI...]" },
    { NULL }
  };

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  g_thread_init (NULL);

  if (!gtk_init_with_args (&argc, &argv,
			_("- Evolution webcal: URI Handler"),
			options,
			GETTEXT_PACKAGE,
			&error)) {
	g_printerr ("%s\n", error->message);
	return 1;
  }

  if (args != NULL) {
    const gchar ** p;

    for (p = (const gchar **) args; *p != NULL; p++) {
      uri_list = g_slist_prepend (uri_list, (char *) *p);
    }
  }

  session = soup_session_async_new ();

  g_idle_add (e_webcal_idle_callback, uri_list);
  gtk_main ();

  return 0;
}
